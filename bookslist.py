# -*- coding: utf-8 -*-
#!/usr/bin/python 
import getbook
import getconfig

def runlist(books_list):
    total_books = len(books_list[1])			## total de libros a descargar
  
    repo_sel = books_list[0]
    list = books_list[1]
    
    #book_name = "jose-hernandez-martin-fierro-es-ed3"		## nombre libro
    
    book_no = 0
    
    for book in list:
      book_no = book_no + 1
      downloader(book, repo_sel, total_books, book_no)
      
    print('\n')

def downloader(book_name, repo_sel, total_books, book_no):
    # Now lets download the book.
    #book_url = repo_sel + book_name + ".html"				## url libro
    book_url = repo_sel
    #print(book_url)
    
    # gets the dir for books from configuration file
    config_file = 'sources.conf'
    books_dir = getconfig.getbooks_dir(config_file)
    
    save_to = r'c:\millennia\library\.html'
    save_to = books_dir + book_name + '.html' # uncomment for linux/os x
    
    print(book_url)
    print(save_to)

    getbook.get(str(book_url), save_to)
    print('\n[100%] (' + str(book_no) + '/' + str(total_books) + ') ' + book_name)


# =============================================
# MAIN
# =============================================
if __name__ == '__main__':
    print('To run Millennia Archive please use: python millennia.py')
