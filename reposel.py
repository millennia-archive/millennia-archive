#!/usr/bin/python 
# -*- coding: utf-8 -*-
import getconfig
import getxml

def search_for_book(book):
    config_file = 'sources.conf'
    repos = getconfig.getrepos(config_file)
    repo_wbook = ['']
    
    #print(repos)
    
    for repo in repos:
        inrepo = getxml.search_book(repo, book)
        #print(config_file, repo, book)
	if inrepo[0] > -1:
	  #print('repo_wbook')
		if repo_wbook[0] == '':
			repo_wbook[0] = repo
		else:
			repo_wbook.append(repo)

    return repo_wbook
    
def read_source():
    print('reading')



# =============================================
# MAIN
# =============================================
if __name__ == '__main__':
    print('To run Millennia Archive please use: python millennia.py')    
