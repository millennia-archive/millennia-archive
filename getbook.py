#!/usr/bin/python 
# -*- coding: utf-8 -*-
#======================================================
import urllib
from xml.dom import minidom
import easycurl
#======================================================

def get_item(url):
    results = easycurl.getpage(url)
    
    return results

def get(book_url, book_out):
   
    result = get_item(book_url)
    if result:
        open(book_out,'wb').write(result)
    else:
        print 'unable to retrieve book.'

# =============================================
# MAIN
# =============================================
if __name__ == '__main__':
    print('To run Millennia Archive please use: python millennia.py')
