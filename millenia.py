#!/usr/bin/python
# -*- coding: utf-8 -*-
#======================================================

## importamos de las librerías
import sys
import getopt

## importamos de millennia
import bookslist
import getconfig
import reposel
import getxml

# =============================================
# MAIN
# =============================================

# set default options
prefs = { 'verbose' : 0}
def getPrefs():
    """
    Parse the commandline options.
    Store options in the global 'prefs' dict,
      and return the remaining arguments.
    """
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hvVsS")
    except:
        print "Invalid options\n"
        usage()
        sys.exit(2)

    for o, a in opts:
        if o == '-h':
            prefs['help'] = 1
        elif o == '-v':
            prefs['verbose'] += 1
        elif o == '-V':
            prefs['version'] = 1
        elif o == '-s':
            prefs['search'] = 1
        elif o == '-S':
            prefs['download'] = 1
    return args
    
def search():
    config_file = 'sources.conf'
    books_dir = getconfig.getbooks_dir(config_file)

    if len(sys.argv) >= 3:
	   books = []
           for i in range (2, len(sys.argv)):
		   books.append(sys.argv[i])
    else:
	   print 'error: no operation specified (use -h for help)'
	   sys.exit(0)

    #repo_sel = "http://www.gutenberg.org/cache/epub/14765/"
    for book in books:
	book_data = list()
	inrepo = reposel.search_for_book(book)           ## repositorio seleccionado
	if inrepo[0] != '':
		print('\n** Millennia Archive Client - Search **')
		print('\nBooks found in repositories:\n')
		for z in range (0, len(inrepo)):
			book_desc = getxml.getbook_desc(inrepo[z], book)
			book_title = getxml.getbook_title(inrepo[z], book)
			book_link = getxml.getbook_link(inrepo[z], book)
			for i in range (0, len(book_desc)):
				print('[' + book_title[i] + ']' + ' in [' + inrepo[z] + ']')
#				print(book_link[i])
				print('    ' + book_desc[i])	
				print('Millennia ID: ' + str(z) + str(i) + '\n')
      

def download():
    config_file = 'sources.conf'
    books_dir = getconfig.getbooks_dir(config_file)

    if len(sys.argv) == 3:
	   print('error: no id specified (use -h for help)')
	   sys.exit(0)
    elif len(sys.argv) > 3:
	   books = []
           #for i in range (2, len(sys.argv)):
	   books.append(sys.argv[2])
    else:
	   print 'error: no operation specified (use -h for help)'
	   sys.exit(0)

    #repo_sel = "http://www.gutenberg.org/cache/epub/14765/"
    for book in books:
	book_not_found = True
	book_data = list()
	inrepo = reposel.search_for_book(book)           ## repositorio seleccionado
	if inrepo[0] != '':
		print('\n** Millennia Archive Client - Download **\n')
#		print('\nBooks found in repositories:\n')
		for z in range (0, len(inrepo)):
			book_desc = getxml.getbook_desc(inrepo[z], book)
			book_title = getxml.getbook_title(inrepo[z], book)
			book_link = getxml.getbook_link(inrepo[z], book)
			for i in range (0, len(book_desc)):
				millennia_id = str(z) + str(i)
				if millennia_id == sys.argv[3]:
				       download_from_repo(book_link[i], books, book_title[i])
 				       book_not_found = False

		if book_not_found: 
			print('No book for id ' + sys.argv[3] + '. Use -s ' + sys.argv[2] + ' to search for the book\'s id.\n')
#					print('[' + book_title[i] + ']' + ' in [' + inrepo[z] + ']')
#					print(book_link[i])
#					print('    ' + book_desc[i])	
#					print('Millennia ID: ' + str(z) + str(i) + '\n')



    #config_file = 'sources.conf'
    #books_dir = getconfig.getbooks_dir(config_file)
    #books = ['pg147654', 'jose-hernandez-martin-fierro-es-ed3']
    
    #repos = list()
    #repo_no = 1
    
    
    #for book in books:
	#inrepo = reposel.search_for_book(book) 		## repositorio seleccionado
	#print(inrepo)
	#if inrepo == '':
	   #print('No book named ' + book + ' was found.\n')
	#else:
	   #print('\nThe book ' + book + ' was found in ' + inrepo)
	   
	   #for repo in repos:
	      #if repo[0] == inrepo:
		  #repo[1].append(book)
	   #else:
	      #books = [book]
	      #repo  = [inrepo, books]
	      #repos.append(repo)
	   
	   #print(repos)
	   
    
    # BY REPOSITORY	   
    
    #for repo in repos:
       #print(repo[0])
       #print(repo[1])
       #download_from_repo(repo[0], repo[1])
       
    print('Remember you can find your downloaded Millennia Archive\'s books in ' + books_dir + '\n')

def download_from_repo(repo_sel, books, book_title):
    config_file = 'sources.conf'
    
    print('\nYou are going to download the following book:\n')
    
    #for book in books:
    print(book_title)
    print(repo_sel)
    
    if ask_ok("\nStart downloading this book? (y/n): ") == True:
      #repo_url = getconfig.getsource_url(config_file, repo_sel)
      #books_dir = getconfig.getbooks_dir(config_file)
      #books_list = [repo_url, books]
      
      #print('\nDownloading books from [' + repo_sel + '] ...')
      #bookslist.runlist(books_list)   
      bookslist.downloader(book_title, repo_sel, 1, 1)

    else:
	print('\nNo book downloaded.')      



def log(msg, priority=1):
    """ print the given message iff the verbose level is high enough """
    if prefs['verbose'] >= priority:
        print >> sys.stderr, msg

def main():
    args = getPrefs()
    log("PREFS: %s" % prefs)
    if "help" in prefs:
        usage()
        sys.exit(2)
    if "version" in prefs:
        version()
        sys.exit(2)
    if "search" in prefs:
        search()
        sys.exit(2)
    if "download" in prefs:
        download()
        sys.exit(2)

def version():
    print('\nMillennia Archive v0.1.0')
    print('Copyright (C) 2010 Alejandro Rean\n')
    
    print('This program can be freely distributed under')
    print('GNU General Public License terms.\n')
    
def usage():
    print('use: millennia <operation> [..]')
    print('use: millennia {-h   help}')
    print('use: millennia {-V   version}')
    print('\nuse \'millennia {-h --help}\' with an operation to see the available options.')

def ask_ok(prompt, retries=4, complaint='Yes or no, please!'):
    while True:
        ok = raw_input(prompt)
        if ok in ('y', 'ye', 'yes', 'Yes'):
            return True
        if ok in ('n', 'no', 'nop', 'nope', 'No'):
            return False
        retries = retries - 1
        if retries < 0:
            raise IOError('refusenik user')
        print complaint

if __name__ == '__main__':
    
    main()
    sys.exit(main())
