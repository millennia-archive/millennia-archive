#!/usr/bin/python 
# -*- coding: utf-8 -*-
import ConfigParser
import fileinput
import sys

config = ConfigParser.RawConfigParser()

def getrepos(config_file):
    config.read(config_file)
    repos = config.get('System', 'repos')
    repos_list = repos.split(',')
    
    return repos_list

def getsource_url(config_file, repo):
    config.read(config_file)

    # getfloat() raises an exception if the value is not a float
    # getint() and getboolean() also do this for their respective types
    #float = config.getfloat('Section1', 'float')
    #int = config.getint('Section1', 'int')
    #print float + int

    # Notice that the next output does not interpolate '%(bar)s' or '%(baz)s'.
    # This is because we are using a RawConfigParser().
    #if config.getboolean('Section1', 'bool'):
    #	print config.get('Section1', 'foo')
    
    return config.get(repo, 'url')

def getbooks_dir(config_file):
    config.read(config_file)
    
    return config.get('System', 'books_dir')   

    
# =============================================
# MAIN
# =============================================
if __name__ == '__main__':
    print('To run Millennia Archive please use: python millennia.py')    
