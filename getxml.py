#!/usr/bin/python 
# -*- coding: utf-8 -*-
import fileinput
import feedparser

def getrepos(config_file):
    config.read(config_file)
    repos = config.get('System', 'repos')
    repos_list = repos.split(',')
    
    return repos_list

def getsource_url(config_file, repo):
    config.read(config_file)

    return config.get(repo, 'url')

def getbooks_dir(config_file):
    config.read(config_file)
    
    return config.get('System', 'books_dir')   

def getbook_desc(repo, book):
	book_desc = ['']

	feed = feedparser.parse( repo )

	for i in range(0, len(feed['entries'])):	
		book_pass = feed.entries[i].title ## este queda así porque busca por título
		book_found = book_pass.find(book)
		if book_found > -1:
			if book_desc[0] == '':
				book_desc[0] = feed.entries[i].description
			else:
				book_desc.append(feed.entries[i].description)

	return book_desc

def getbook_link(repo, book):
	book_link = ['']

	feed = feedparser.parse( repo )

	for i in range(0, len(feed['entries'])):	
		book_pass = feed.entries[i].title ## este queda así porque busca por título
		book_found = book_pass.find(book)
		if book_found > -1:
			if book_link[0] == '':
				book_link[0] = feed.entries[i].link
			else:
				book_link.append(feed.entries[i].link)

	return book_link

def getbook_title(repo, book):
	book_title = ['']

	feed = feedparser.parse( repo )

	for i in range(0, len(feed['entries'])):	
		book_pass = feed.entries[i].title ## este queda así porque busca por título
		book_found = book_pass.find(book)
		if book_found > -1:
			if book_title[0] == '':
				book_title[0] = feed.entries[i].title
			else:
				book_title.append(feed.entries[i].title)

	return book_title

def search_book(repo, book):
	book_id = [-1]

	feed = feedparser.parse( repo )
	for i in range(0, len(feed['entries'])):	
		book_pass = feed.entries[i].title
		book_found = book_pass.find(book)

		if book_found > -1:
			if book_id[0] == -1:
				book_id = [i]
			else:
				book_id.append(i)

			return book_id

	return book_id

    
# =============================================
# MAIN
# =============================================
if __name__ == '__main__':
    print('To run Millennia Archive please use: python millennia.py')    
