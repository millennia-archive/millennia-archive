import subprocess,StringIO
import pycurl 
Debug=0

def getpage(url,user='',password='',proxy='',puser='',ppass=''):
   strio=StringIO.StringIO() 
   curlobj=pycurl.Curl()
   if Debug: curlobj.setopt(pycurl.VERBOSE,1)
   if proxy:
         curlobj.setopt(pycurl.PROXYPORT,80)
         curlobj.setopt(pycurl.PROXY, proxy)
         curlobj.setopt(pycurl.PROXYUSERPWD, puser+':'+ppass)
         curlobj.setopt(pycurl.HTTPAUTH,pycurl.HTTPAUTH_ANY)
   if user:
      curlobj.setopt(pycurl.HTTPAUTH,pycurl.HTTPAUTH_ANY)
      curlobj.setopt(pycurl.USERPWD, user+':'+password) 
   curlobj.setopt(pycurl.HTTPHEADER, [
      "User-Agent: Mozilla/5.001 (windows; U; NT4.0; en-us) Gecko/25250101",
      "Accept-Language: en-us,en;q=0.5",
        ])

#   curlobj.setopt(pycurl.HTTPHEADER, ["User-Agent: Mozilla/5.001 (windows; U; NT4.0; en-us) Gecko/25250101", "Agent: "])
   curlobj.setopt(pycurl.URL, url)
   curlobj.setopt(pycurl.WRITEFUNCTION, strio.write)
   curlobj.perform() 
   curlobj.close()
   return strio.getvalue()
